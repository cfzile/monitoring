FROM python:3-onbuild

ENV PYTHONUNBUFFERED 1

WORKDIR .

COPY . .
RUN python3 -m venv venv
RUN pip3 install python-a2s
RUN pip install -r requirements.txt

