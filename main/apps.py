from django.apps import AppConfig

from main.models import PremiumPlace


class MainConfig(AppConfig):
    name = 'main'


def get_premium_servers():
    premium_servers = []

    for place in PremiumPlace.objects.all():
        premium_servers.append(place.server)

    return premium_servers
