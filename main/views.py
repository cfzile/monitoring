import json
from audioop import reverse

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader, RequestContext
import a2s
from django.utils import timezone
from django.urls import reverse

import main.constance as constance
from main.apps import get_premium_servers

from main.forms import UserForm
from main.models import Server, Profile, PremiumPlace, Order


def home(request, page=1):
    template = loader.get_template('index.html')

    list_servers = Server.objects.all().filter(ordered=False)
    orders = Order.objects.all().order_by('place_number').filter(place=1, date_from__gte=timezone.localtime(),
                                                                 date_to__lte=timezone.localtime())

    pages = range(1, len(list_servers) // 100 + min(1, len(list_servers) % 100) + 1)

    list_servers = list_servers[max(0, 100 * (page - 1) - len(orders)):(100 * page - len(orders))]

    if len(list_servers) == 0 and page != 1:
        raise Http404()

    servers = []
    pos = 1

    for order in orders:
        server = order.server
        server.position_in_table = pos
        server.update_info()
        servers.append(server)

        pos += 1

    for server in list_servers:

        if server.update_info():
            server.position_in_table = pos
            servers.append(server)

        pos += 1

    response = HttpResponse(template.render({'Title': constance.SITE_MONITORING_CSS_V_34_NAME,
                                             'constance': constance,
                                             'page': page,
                                             'pages': pages,
                                             'servers': servers,
                                             'premium_servers': get_premium_servers(),
                                             'selected_monitoring': 'selected'}, request))

    return response


def sign_up(request):
    if request.user.is_authenticated:
        return redirect('/')

    registered = False
    errors = None

    user_form = UserForm()

    if request.method == 'POST':

        user_form = UserForm(request.POST)

        if user_form.is_valid():

            user = user_form.save(commit=False)
            user.set_password(user_form.cleaned_data['password'])
            user.save()
            Profile.objects.create(user=user, servers=[])

            registered = True

        else:
            errors = user_form.errors

    return render(request, "index.html", {'Title': constance.SITE_SIGN_UP_NAME,
                                          'premium_servers': get_premium_servers(),
                                          'user_form': user_form,
                                          'errors': errors,
                                          'constance': constance,
                                          'registered': registered})


def sign_in(request):
    if request.user.is_authenticated:
        return redirect('/')

    template = loader.get_template('index.html')
    errors = []
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('/')
        else:
            errors = user.errors
            # errors = {'Ошибка:': [ERROR_MESSAGE_DONT]}

    return HttpResponse(template.render({'Title': constance.SITE_SIGN_IN_NAME,
                                         'premium_servers': get_premium_servers(),
                                         'constance': constance,
                                         'errors': errors}, request))


def sign_out(request):
    if request.user.is_authenticated:
        logout(request)

    return redirect('/')


def handler404(request):
    response = render(request, 'index.html', {})
    response.status_code = 404
    return response


def profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    return render(request, "index.html", {"Title": user.username, "profile_of_user": user})


def add_server(request):
    if constance.SERVER_ADDED:
        constance.SERVER_ADDED = False
        return HttpResponse(render(request, "index.html", {'Title': constance.SITE_SERVER_ADD_NAME,
                                                           'premium_servers': get_premium_servers(),
                                                           'server_added': True,
                                                           'selected_add_server': 'selected',
                                                           'constance': constance}))

    errors = []

    if request.method == 'POST':

        ip = request.POST.get("ip")
        port = request.POST.get("port")

        if len(ip) < 10 or len(port) > 5 or len(port) < 2:
            errors = {'Ошибка:': [constance.NON_CORRECT_DATA]}
        else:
            try:
                Server.objects.get(ip=ip, port=port)
                errors = {'Ошибка:': [constance.SUCH_SERVER_EXIST("%s:%s" % (ip, port))]}
            except:
                try:
                    server = Server.objects.create(ip=ip, port=port, position_in_table=1000)
                    if server.update_info():
                        constance.SERVER_ADDED = True
                    else:
                        server.delete()
                        raise
                except:
                    errors = {'Ошибка:': [constance.NON_CORRECT_DATA_OR_SERVER_DONT_RUN(ip, port)]}

    if constance.SERVER_ADDED:
        return redirect(reverse('add_server'))

    return HttpResponse(render(request, "index.html", {'Title': constance.SITE_SERVER_ADD_NAME,
                                                       'premium_servers': get_premium_servers(),
                                                       'server_added': constance.SERVER_ADDED,
                                                       'errors': errors,
                                                       'selected_add_server': 'selected',
                                                       'constance': constance}))


def buy_place(request):
    errors = []
    data_table = []

    pos = -3

    for i in range(3):
        t = int(1000 * timezone.localtime().timestamp())
        lst = list(PremiumPlace.objects.order_by('date_to').filter(place_number=i + 1))
        if len(lst) != 0:
            t = lst[-1].date_to.timetuple() + timezone.timedelta(1)
        data_table.append([pos, 'правый стенд место #%d' % (i + 1), 600, t])
        pos += 1

    for i in range(1, 101):
        lst = list(Order.objects.order_by('date_to').filter(place_number=i))

        if len(lst) == 0:
            data_table.append([pos, 'место #%d в таблице' % i, constance.PRICES_TABLE()[i - 1],
                               int(1000 * timezone.localtime().timestamp())])
        else:
            data_table.append([pos, 'место #%d в таблице' % i, constance.PRICES_TABLE()[i - 1],
                               int(1000 * (lst[-1].date_to.timestamp() + timezone.timedelta(1)))])

        pos += 1

    return HttpResponse(render(request, "index.html", {'Title': constance.SITE_BUY_PLACE_NAME,
                                                       'errors': errors,
                                                       'data_table': data_table,
                                                       'selected_buy_place': 'selected',
                                                       'constance': constance}))


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_expiry_session():
    w = timezone.localtime().weekday()

    expiry_session = timezone.timedelta(weeks=1).total_seconds() - (
            timezone.localtime().time().hour * 60 * 60 + timezone.localtime().time().minute * 60 + timezone.localtime().time().second) - w * 24 * 60 * 60

    return expiry_session


def add_voice(request):
    if request.method == 'GET':
        return redirect("/")

    server = get_object_or_404(Server, id=request.POST['server'])

    if not request.session.has_key('ip'):
        request.session['ip'] = get_client_ip(request)
        request.session['add_voice'] = False
        request.session.set_expiry(get_expiry_session())
    else:
        if request.session['add_voice']:
            return HttpResponse(json.dumps({'results': server.num_points}), content_type='application/json')

    server.num_points += 1
    server.save()

    data = {
        'results': server.num_points
    }

    request.session['add_voice'] = True

    return HttpResponse(json.dumps(data), content_type='application/json')
