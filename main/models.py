from datetime import datetime

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone, dateformat
import a2s


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    servers = ArrayField(models.IntegerField(null=True, blank=True), null=True)

    def __str__(self):
        return self.user.username


class Server(models.Model):
    ip = models.CharField(max_length=15)
    port = models.IntegerField()
    gameType = models.CharField(max_length=20, null=True)
    map_name = models.CharField(max_length=50, null=True)
    gameVersion = models.CharField(max_length=15, null=True)
    name = models.CharField(max_length=100, null=True)
    max_players = models.IntegerField(null=True)
    player_count = models.IntegerField(null=True)
    num_points = models.IntegerField(default=0)
    state = models.IntegerField(null=True)
    status = models.CharField(default="disconnected", max_length=20)
    owner = models.ForeignKey('Profile', on_delete=models.CASCADE, null=True)
    position_in_table = models.IntegerField()
    date_last_update = models.DateTimeField(default=timezone.localtime)
    ordered = models.BooleanField(default=False)

    def update_info(self):

        if timezone.localtime().timestamp() - self.date_last_update.timestamp() < 60 and (self.name is not None):
            return True

        if Order.objects.filter(place=1, server=self.id, date_from__gte=timezone.localtime(),
                                date_to__lte=timezone.localtime()):
            self.ordered = True
        else:
            self.ordered = False

        try:

            address = (self.ip, int(self.port))
            info = a2s.info(address, timeout=1)

            self.player_count = info.player_count
            self.max_players = info.max_players
            self.map_name = info.map_name
            self.name = info.server_name
            self.status = "connected"
            self.date_last_update = timezone.localtime()

            self.save()

        except:
            self.status = "disconnected"
            self.save()

            return False

        return True


class PremiumPlace(models.Model):
    server = models.ForeignKey('Server', on_delete=models.CASCADE, null=True)
    place_number = models.IntegerField(default=0)
    date_from = models.DateField(default=timezone.localtime)
    date_to = models.DateField(default=timezone.localtime)


class Order(models.Model):
    server = models.ForeignKey('Server', on_delete=models.CASCADE, null=True)
    date_from = models.DateField(default=timezone.localtime)
    date_to = models.DateField(default=timezone.localtime)
    place = models.IntegerField()
    place_number = models.IntegerField()