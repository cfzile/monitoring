from django.test import TestCase

from main.models import Server


class AddServer(TestCase):
    ip = "11.11.11.11"
    port = 5448

    def setUp(self):
        Server.objects.create(ip=self.ip, port=self.port, position_in_table=0).update_info()

    def test_find_server(self):
        self.assertEqual(Server.objects.filter(ip=self.ip, port=self.port).count(), 0, "Added disconnected server.")
