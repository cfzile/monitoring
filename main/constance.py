SITE_MONITORING_CSS_V_34_NAME = 'Мониторинг серверов CSSv34'
SITE_SIGN_IN_NAME = 'Вход'
SITE_SIGN_UP_NAME = 'Регистрация'
SITE_SERVER_ADD_NAME = 'Добавить сервер'
NON_CORRECT_DATA = 'Введены некорректные данные'
SERVER_ADDED = False
SITE_BUY_PLACE_NAME = 'Купить место'


def PRICES_TABLE():
    prices_table = []

    cur = 500

    for i in range(100):

        if i / 100. <= 0.3:
            prices_table.append(cur)
        elif i / 100. <= 0.6:
            prices_table.append(cur - 150)
        else:
            prices_table.append(cur - 300)

    return prices_table


def SUCH_SERVER_EXIST(addr=''):
    return 'Сервер с адресом %s уже добавлен' % addr


def NON_CORRECT_DATA_OR_SERVER_DONT_RUN(ip, port):
    return NON_CORRECT_DATA + (" %s:%s" % (ip, str(port))) + " или сервер не запущен"


