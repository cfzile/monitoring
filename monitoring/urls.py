"""monitoring URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from main import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', views.home),
    path('monitor/<int:page>', views.home, name='monitoring_table'),
    # path('sign_in', views.sign_in, name='sign_in'),
    # path('sign_up', views.sign_up, name='sign_up'),
    # path('sign_out', views.sign_out, name='sign_out'),
    # path('u<int:user_id>', views.profile, name='profile'),
    path('add_server', views.add_server, name='add_server'),
    path('buy_place', views.buy_place, name='buy_place'),
    path('add_voice', views.add_voice, name='add_voice')
]
